Il gruppo si è posto l’obiettivo di realizzare un’applicazione che porta gli utenti a preferire l’acquisto di prodotti nei negozi invece che online. Tale obiettivo viene perseguito dando la possibilità ai negozianti di offrire dei coupon per usufruire degli sconti su dei prodotti.  Un coupon consiste in un codice identificato da tre variabili, ovvero il cliente, il negozio e il prodotto. Dunque, per lo stesso negozio un determinato utente non può usufruire più volte della stessa offerta, ma potrà usufruire di altre offerte presso lo stesso negozio o altri negozi. La suddetta app dovrà permettere agli utenti, divisi in due categorie (clienti e negozianti) di: 
* Ricercare i negozi nelle vicinanze o nelle città interessate;
* Scegliere il negozio dal quale si vuole acquistare;
* Richiedere un coupon che servirà per ricevere lo sconto su alcuni prodotti;
* Pubblicizzare il proprio negozio;
* Offrire degli sconti sui propri prodotti;
* Visualizzare i coupon utilizzati. 
