package sms1920.buybuycoupon.ui;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import sms1920.buybuycoupon.HomeActivity;
import sms1920.buybuycoupon.MainActivity;
import sms1920.buybuycoupon.NegoziAdapter;
import sms1920.buybuycoupon.R;

public class HomeFragment extends Fragment {

    public static NegoziAdapter getNegoziAdapter() {
        return negoziAdapter;
    }

    private static NegoziAdapter negoziAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        //prassi del fragment

        if(savedInstanceState!=null)
            //rende visibile l'icona della ricerca
            HomeActivity.getSearchItem().setVisible(true);

        TextView cittaTv = (TextView) root.findViewById(R.id.cittaTv);
        cittaTv.setText(getActivity().getIntent().getExtras().getString("citta"));

        final TextView cambiaCitta = root.findViewById(R.id.cambiaCitta);

        cambiaCitta.setPaintFlags(cambiaCitta.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        //rende la textview sottolineata per rendere l'effetto "link cliccabile"

        cambiaCitta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), MainActivity.class);
                //la textview cambiaCitta porta alla main per cambiare città
                startActivity(intent);
                try {
                    super.finalize();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });

        negoziAdapter = HomeActivity.getNegoziAdapter();
        //inizializza l'adapter per popolare la recyclerview con i dati dei negozi

        RecyclerView negoziR = root.findViewById(R.id.negozi);
        //lista delle card con i negozi

        negoziR.setLayoutManager(new LinearLayoutManager(getContext()));
        negoziR.setItemAnimator(new DefaultItemAnimator());
        negoziR.setAdapter(negoziAdapter);
        //collegamento del recyclerview con l'adapter con i dati dei negozi

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(HomeActivity.getSearchItem()!=null)
            //rende visibile l'icona della ricerca
            if(!HomeActivity.getSearchItem().isVisible())
                HomeActivity.getSearchItem().setVisible(true);
    }
}