package sms1920.buybuycoupon.ui;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.ObjectKey;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import sms1920.buybuycoupon.AddCoupon;
import sms1920.buybuycoupon.HomeActivity;
import sms1920.buybuycoupon.MainActivity;
import sms1920.buybuycoupon.R;
import sms1920.buybuycoupon.SignUpActivity;
import sms1920.buybuycoupon.SplashActivity;

public class ProfileFragment extends Fragment {

    private ToggleButton TB_modifica;
    private TextView nome;
    private TextView data_nascita;
    private TextView email;
    private ImageView immagine;
    private Button foto;
    private Button forgotPassword;
    private FirebaseAuth fAuth;
    private FirebaseFirestore fStore;
    private FirebaseStorage storage;
    private FirebaseUser user;
    private String path="nothing";
    private static final int PICK_IMAGE = 1;
    private String newPath="";
    private Bitmap bitmap;
    private String uid;
    private ImageView image;
    private Button delete;
    private Dialog mDialog;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_profile, container, false);

        //nasconde l'icona della ricerca
        if(HomeActivity.getSearchItem().isVisible())
            HomeActivity.getSearchItem().setVisible(false);

        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        user = fAuth.getCurrentUser();

        //inizializzazione views
        foto = root.findViewById(R.id.BT_ModificaFoto);
        foto.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        TB_modifica = root.findViewById(R.id.TB_modifica);
        nome = root.findViewById(R.id.nome);
        data_nascita = root.findViewById(R.id.dataNascita);
        email = root.findViewById(R.id.email);
        forgotPassword = root.findViewById(R.id.forgotPassword);
        forgotPassword.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        immagine = root.findViewById(R.id.ImageViewProfilo);
        delete = root.findViewById(R.id.delAcc);
        delete.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        //mostra la progress bar durante il caricamento dell'immagine del profilo
        final ProgressBar progressBar = root.findViewById(R.id.progressBarProPic);

        final NavigationView navigationView = getActivity().findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        image = header.findViewById(R.id.imageView);

        nome.setText(user.getDisplayName());
        email.setText(user.getEmail());

        final Query users = fStore.collection("users")
                .whereEqualTo("email", user.getEmail());
        //imposta la query per recuperare i dati dell'utente collegato
        //per recuperare i dati esatti fa un confronto sull'email

        users.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for(QueryDocumentSnapshot document : task.getResult()) {
                        uid = document.getId();
                        data_nascita.setText(document.getString("data"));
                        path = document.get("immagine").toString();
                        if(path.equals("nothing"))
                            progressBar.setVisibility(View.INVISIBLE);
                    }

                    StorageReference fileRef = storage.getReferenceFromUrl("gs://bbcoupon-e99f6.appspot.com").child(path);
                    //imposta il percorso dell'immagine del profilo per recuperarla dallo storage remoto

                    if(!path.equals("nothing"))
                        try {
                            final File localFile = File.createTempFile("images", "jpg");
                            //crea un file locale temporaneo per salvare l'immagine del profilo

                            fileRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                //recupera il file dallo storage
                                @Override
                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                    Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                                    Glide.with(getContext()).load(bitmap).listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progressBar.setVisibility(View.INVISIBLE);
                                            return false;
                                        }
                                    }).centerCrop().into(immagine);
                                    //imposta l'immagine nella view del menu laterale sinistro
                                }
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                }
            }
        });

        //prima della pressione del toggle button "modifica"
        //i bottoni e le editText non devono essere modificabili
        foto.setEnabled(false);
        nome.setEnabled(false);
        data_nascita.setEnabled(false);
        email.setEnabled(false);
        forgotPassword.setEnabled(false);
        delete.setEnabled(false);

        //gestione modifica dell'immagine del profilo
        foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //verifica del permesso di accesso allo storage
                if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},PICK_IMAGE);
                else {
                    //se si ha il permesso di accedere allo storage,
                    //allora al tocco del pulsante apre l'internt per
                    //scegliere l'immagine da caricare.
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_IMAGE);
                }
            }
        });

        delete.setBackgroundColor(getResources().getColor(R.color.RED));
        //gestione dell'eliminazione dell'account
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dialog che richiede se si vuole effeettivamente eliminare l'account
                mDialog = new Dialog(getContext());
                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mDialog.setContentView(R.layout.quit_dialog);

                //personalizzazione dialog
                mDialog.setTitle(R.string.deleteDialog);
                TextView warning = mDialog.findViewById(R.id.textView2);
                TextView description = mDialog.findViewById(R.id.textView3);
                warning.setText(getString(R.string.warning));
                description.setText(getString(R.string.dataLosing));

                Button pos = mDialog.findViewById(R.id.pos);
                Button neg = mDialog.findViewById(R.id.neg);

                pos.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                pos.setTextSize(13);
                neg.setBackgroundColor(getResources().getColor(R.color.RED));
                neg.setTextSize(13);

                pos.setOnClickListener(new View.OnClickListener() {
                    //conferma l'intenzione di voler eliminare l'account
                    @Override
                    public void onClick(View v) {
                        deletion();
                    }
                });

                neg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //chiude semplicemente il dialog
                        mDialog.dismiss();
                    }
                });

                mDialog.show();
            }
        });

        TB_modifica.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    //toggle button attivo
                    foto.setEnabled(true);
                    email.setEnabled(true);
                    forgotPassword.setEnabled(true);
                    delete.setEnabled(true);
                }

                else
                {
                    //toggle button disattivo, salvataggio eventuali modifiche
                    foto.setEnabled(false);
                    email.setEnabled(false);
                    forgotPassword.setEnabled(false);
                    delete.setEnabled(false);

                    String emailTxt = email.getText().toString().trim();

                    if(!emailTxt.equals(""))
                        //controlla che l'editText non sia vuota
                        //prima di aggiornare il campo nel db
                        user.updateEmail(emailTxt).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                //notifica di modifiche avvenute
                                Toast.makeText(getContext(),getString(R.string.editSaved),Toast.LENGTH_SHORT).show();
                            }
                        });
                }
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fAuth.sendPasswordResetEmail(user.getEmail()).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful())
                            //invio e-mail per recupero password
                            Toast.makeText(getContext(),getResources().getString(R.string.check_mail),Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        return root;
    }

    private void deletion() {
        final String userUid = user.getUid();

        user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            //elimina l'utente dall'auth
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                fStore.collection("users").document(userUid).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                    //elimina i dati dell'utente dal database
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(HomeActivity.getNegozio()!=null)
                            //se l'utente corrente è un negoziante, bisogna eliminare dal db anche il negozio
                            fStore.collection("shops").document(userUid).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    end();
                                }
                            });
                        else end();
                    }
                });
            }
        });
    }


    //semplicemente chiude il fragment e l'activity tornando alla main
    private void end() {
        mDialog.dismiss();
        startActivity(new Intent(getContext(), MainActivity.class));
        Toast.makeText(getContext(),getString(R.string.deletionDone),Toast.LENGTH_LONG).show();
        getActivity().finish();
    }

    /**
     * Metodo che gestisce la risposta dell'utente alla richiesta del permesso di accesso all'archivio
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, PICK_IMAGE);
            }
    }


    /**
     * operazioni svolte dopo l'intent della galleria
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && null != data) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();

                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                decodeFile(picturePath);

                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRef = storage.getReference();
                newPath = "images/"+ selectedImage.getLastPathSegment();
                StorageReference riversRef = storageRef.child(newPath);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                UploadTask uploadTask = riversRef.putFile(selectedImage);

                // caricamento immagine nello storage
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.e("upload:","failed");
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Map<String, Object> map = new HashMap<>();

                        map.put("immagine",newPath);

                        //aggiornamento dell'immagine nel db
                        fStore.collection("users").document(uid)
                                .update(map);

                        Log.e("upload:","success");
                    }
                });
            }

        }
    }

    public void decodeFile(String filePath) {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        bitmap = BitmapFactory.decodeFile(filePath, o2);

        Glide.with(getActivity()).load(bitmap).centerCrop().into(immagine);
        Glide.with(getActivity()).load(bitmap).centerCrop().into(image);
    }
}