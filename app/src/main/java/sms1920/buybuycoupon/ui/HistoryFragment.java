package sms1920.buybuycoupon.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import sms1920.buybuycoupon.CouponAdapter;
import sms1920.buybuycoupon.HomeActivity;
import sms1920.buybuycoupon.R;
import sms1920.buybuycoupon.model.Coupon;

public class HistoryFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_history, container, false);

        //nasconde l'icona della ricerca
        if(HomeActivity.getSearchItem().isVisible())
            HomeActivity.getSearchItem().setVisible(false);

        final RecyclerView coupon = root.findViewById(R.id.recyclerViewHistory);

        //recupera la lista dei coupon usati dalla home
        ArrayList<Coupon> coupons = HomeActivity.getUsedCoupons();

        //inizializza l'adapter e la recyclerview con i coupon usati
        CouponAdapter couponAdapter = new CouponAdapter(coupons, "storico");
        coupon.setLayoutManager(new LinearLayoutManager(getContext()));
        coupon.setItemAnimator(new DefaultItemAnimator());
        coupon.setAdapter(couponAdapter);

        return root;
    }
}