package sms1920.buybuycoupon.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

import sms1920.buybuycoupon.DarkModeActivity;
import sms1920.buybuycoupon.HomeActivity;
import sms1920.buybuycoupon.R;

public class ToolsFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_tools, container, false);

        //nasconde l'icona della ricerca
        if(HomeActivity.getSearchItem().isVisible())
            HomeActivity.getSearchItem().setVisible(false);

        //inizializzazione views
        Button bluetooth= root.findViewById(R.id.BT_bluetooth);
        bluetooth.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        Button gps= root.findViewById(R.id.BT_GPS);
        gps.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        final Switch darkModeSwitch = (Switch) root.findViewById(R.id.dark_theme_switch);

        //chiamata alle impostazioni del bluetooth per attivarlo/disattivarlo
        bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
                startActivity(i);
            }
        });

        //chiamata alle impostazioni del gps
        gps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            }
        });

        if(AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES) {
            darkModeSwitch.setChecked(true);
        }

        //gestione dello switch per le modalità di visualizzazione
        darkModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(darkModeSwitch.isChecked()) { //controllo se lo switch è attivo
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES); //modalità notturna attivata
                    saveData(true);
                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO); //modalità notturna disattivata
                    saveData(false);
                }
            }
        });

        return root;
    }

    //salvataggio dei dati
    public void saveData(final boolean b) {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(DarkModeActivity.SHARED_PREFS, getContext().MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean(DarkModeActivity.DARK_MODE, b);

        editor.apply();

        Log.e("saveData", b ? "true" : "false");
    }
}