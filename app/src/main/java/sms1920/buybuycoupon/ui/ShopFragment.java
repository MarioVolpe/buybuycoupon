package sms1920.buybuycoupon.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import sms1920.buybuycoupon.AddCoupon;
import sms1920.buybuycoupon.CouponAdapter;
import sms1920.buybuycoupon.HomeActivity;
import sms1920.buybuycoupon.R;
import sms1920.buybuycoupon.SplashActivity;
import sms1920.buybuycoupon.model.Coupon;
import sms1920.buybuycoupon.model.Negozio;

public class ShopFragment extends Fragment {

    private ImageView imageView;
    private EditText nome;
    private EditText descrizione;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private Negozio negozio;
    private FirebaseFirestore fStore;
    private Button editShopPic;
    private String nomeTxt="";
    private String descTxt="";
    private static final int PICK_IMAGE = 1;
    private Bitmap bitmap;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.activity_negozio, container, false);

        //nasconde l'icona della ricerca
        if(HomeActivity.getSearchItem().isVisible())
            HomeActivity.getSearchItem().setVisible(false);

        //inizializzazione views
        imageView = root.findViewById(R.id.imgNegozio);
        nome = root.findViewById(R.id.nomeNegozio);
        descrizione = root.findViewById(R.id.descrizioneNegozio);
        TextView apriMap = root.findViewById(R.id.descrizioneMapButton);
        final RecyclerView coupon = root.findViewById(R.id.coupon);
        ToggleButton toggleButton = root.findViewById(R.id.edit_shop);
        editShopPic = root.findViewById(R.id.edit_shopPic);
        editShopPic.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        FloatingActionButton floatingActionButton = root.findViewById(R.id.floatingActionButton);

        //rende visibili alcune views che sono nascoste in negozioActivity
        toggleButton.setVisibility(View.VISIBLE);
        editShopPic.setVisibility(View.VISIBLE);
        floatingActionButton.setVisibility(View.VISIBLE);
        ImageButton mapButtn = root.findViewById(R.id.mapButton);
        mapButtn.setVisibility(View.INVISIBLE);
        apriMap.setVisibility(View.INVISIBLE);

        fStore = FirebaseFirestore.getInstance();

        //recupera dalla home il negozio del quale mostrare i dettagli
        negozio = HomeActivity.getNegozio();
        //e i coupon creati dal negoziante (se presenti)
        ArrayList<Coupon> coupons = HomeActivity.getCoupons();

        //inizializza le textview con i dati del negozio
        nome.setText(negozio.getNomeNegozio());
        descrizione.setText(negozio.getDescrizione());

        StorageReference fileRef = storage.getReferenceFromUrl("gs://bbcoupon-e99f6.appspot.com").child(negozio.getImmagine());
        //imposta il percorso dell'immagine del negozio per recuperarla dallo storage remoto

        try {
            final File localFile = File.createTempFile("images", "jpg");
            //crea un file locale temporaneo per salvare l'immagine del negozio

            fileRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    Glide.with(getActivity()).load(bitmap).centerCrop().into(imageView);
                    //imposta l'immagine nell'imageview che sta in alto.
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

        //inizializza l'adapter dei coupon e di conseguenza la recyclerview
        //nella quale sono contenuti i coupon creati dal negoziante
        CouponAdapter couponAdapter = new CouponAdapter(coupons, "edit");
        coupon.setLayoutManager(new LinearLayoutManager(getContext()));
        coupon.setItemAnimator(new DefaultItemAnimator());
        coupon.setAdapter(couponAdapter);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //se toccato il FAB, viene chiamata l'activity di aggiunta del coupon
                Intent intent = new Intent(getContext(), AddCoupon.class);
                intent.putExtra("chiamante","add");
                String citta = HomeActivity.getCitta();
                intent.putExtra("citta",citta);
                startActivity(intent);
            }
        });

        //disabilita i bottoni quando prima del tocco del togglebutton
        editShopPic.setEnabled(false);
        nome.setEnabled(false);
        descrizione.setEnabled(false);

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                //togglebutton attivo
                {
                    nome.setEnabled(true);
                    descrizione.setEnabled(true);
                    editShopPic.setEnabled(true);

                    editShopPic.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //verifica del permesso di accesso allo storage
                            if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},PICK_IMAGE);
                            else
                            {//se si ha il permesso di accedere allo storage,
                                //allora al tocco del pulsante apre l'internt per
                                //scegliere l'immagine da caricare.
                                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(galleryIntent, PICK_IMAGE);
                            }
                        }
                    });
                }

                else
                {
                    //togglebutton disattivato
                    nome.setEnabled(false);
                    descrizione.setEnabled(false);
                    editShopPic.setEnabled(false);
                    nomeTxt = nome.getText().toString().trim();
                    descTxt = descrizione.getText().toString().trim();

                    if(nomeTxt.equals("")||descTxt.equals(""))
                        Toast.makeText(getContext(),getString(R.string.insert_values),Toast.LENGTH_SHORT).show();
                    //se i campi non sono vuoti allora si effettua l'aggiornamento dei dati del negozio
                    else update();

                }
            }
        });

        return root;
    }

    /**
     * Metodo che gestisce la risposta dell'utente alla richiesta del permesso di accesso all'archivio
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //se è stato concesso il permesso cambio il valore del flag

            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, PICK_IMAGE);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && null != data) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();

                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                decodeFile(picturePath);

                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRef = storage.getReference();
                //salvo il percorso dell'immagine nel campo "immagine" del negozio
                negozio.setImmagine("images/"+ selectedImage.getLastPathSegment());
                StorageReference riversRef = storageRef.child(negozio.getImmagine());

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                UploadTask uploadTask = riversRef.putFile(selectedImage);

                // Register observers to listen for when the download is done or if it fails
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.e("upload:","failed");
                        // Handle unsuccessful uploads
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Log.e("upload:","success");
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                        // ...
                    }
                });
            }

        }
    }

    private void decodeFile(String filePath) {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        bitmap = BitmapFactory.decodeFile(filePath, o2);

        imageView.setImageBitmap(bitmap);
    }

    private void update() {
        // Crea un mappa con i dati del negozio
        Map<String, Object> negoziodb = new HashMap<>();
        negoziodb.put("nome", nomeTxt);
        negoziodb.put("descrizione", descTxt);
        negoziodb.put("immagine", negozio.getImmagine());

        // aggiorna il negozio con i nuovi dati
        fStore.collection("shops").document(negozio.getIdNegozio())
                .update(negoziodb)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Intent intent = new Intent(getContext(),SplashActivity.class);
                        intent.putExtra("destinazione","home");
                        intent.putExtra("citta",HomeActivity.getCitta());
                        startActivity(intent);
                        getActivity().finish();
                    }
                });
    }
}