package sms1920.buybuycoupon;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import sms1920.buybuycoupon.model.Negozio;
import sms1920.buybuycoupon.model.Utente;

public class SignUpActivityShop extends AppCompatActivity {

    private static final int PICK_IMAGE = 1;
    private ImageView image;
    private Bitmap bitmap;
    //variabile di controllo per agire in caso di permesso concesso o meno
    private FirebaseFirestore fStore;
    private TextView nomeT;
    private TextView descrizioneT;
    private TextView viaT;
    private TextView civicoT;
    private Utente utente;
    private Negozio negozio = new Negozio();
    private boolean immagineNonCaricata = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signupshop);

        utente = (Utente) getIntent().getExtras().get("utente");
        //recupero dei dati dall'activity chiamante
        negozio.setIdNegozio(utente.getIdUtente());

        nomeT = findViewById(R.id.nome_negozio);
        descrizioneT = findViewById(R.id.descrizioneNegozio);
        viaT = findViewById(R.id.indirizzoNegozio);
        civicoT = findViewById(R.id.civicoNegozio);
        image = findViewById(R.id.imgNegozio);
        Button upload = findViewById(R.id.UploadFile);
        Button avanti = findViewById(R.id.regNegoziante);
        //collegamento degli elementi dell'interfaccia con gli oggetti locali

        upload.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        avanti.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        String[] countries = getResources().getStringArray(R.array.citta);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, countries);
        final AutoCompleteTextView cittaT = findViewById(R.id.cittaNegozio);
        cittaT.setAdapter(adapter);
        //collegamento dell'adapter all'AutcompleteTextView per la scelta della città

        final Spinner settore = findViewById(R.id.spinnerSettore);
        final ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<>(this,R.layout.row,getResources().getStringArray(R.array.settori));
        settore.setAdapter(stringArrayAdapter);
        //collegamento dello spinner all'adapter per la scelta del settore

        settore.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            //spinner contenente i settori possibili del negozio
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                //aggiornamento della view in base al settore scelto nello spinner
                TextView txt= arg1.findViewById(R.id.rowtext);
                updateSettore(txt.getText().toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
            }
        });


        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ContextCompat.checkSelfPermission(SignUpActivityShop.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(SignUpActivityShop.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);
                }
                else
                {
                    //se abbiamo il permesso di accedere allo storage si può procedere con l'intent
                    //per la scelta dell'immagine da caricare
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_IMAGE);
                }
            }
        });

        fStore = FirebaseFirestore.getInstance();

        avanti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String citta = cittaT.getText().toString().trim();
                String nome = nomeT.getText().toString().trim();
                String descrizione = descrizioneT.getText().toString().trim();
                String via = viaT.getText().toString().trim();
                String civico = civicoT.getText().toString().trim();
                //recupero dei dati dalle view

                //controlli sull'input
                if(immagineNonCaricata){
                    Toast.makeText(SignUpActivityShop.this,getResources().getString(R.string.imgNotLoaded),Toast.LENGTH_SHORT).show();
                    return;
                }

                if(citta.equals("")){
                    Toast.makeText(SignUpActivityShop.this,getResources().getString(R.string.cityNotSet),Toast.LENGTH_SHORT).show();
                    return;
                } else negozio.setCitta(citta);

                if(nome.equals("")){
                    Toast.makeText(SignUpActivityShop.this,getResources().getString(R.string.nameNotSet),Toast.LENGTH_SHORT).show();
                    return;
                } else negozio.setNomeNegozio(nome);

                if(descrizione.equals("")){
                    Toast.makeText(SignUpActivityShop.this,getResources().getString(R.string.descNotSet),Toast.LENGTH_SHORT).show();
                    return;
                } else negozio.setDescrizione(descrizione);

                if(via.equals("")){
                    Toast.makeText(SignUpActivityShop.this,getResources().getString(R.string.addressNotSet),Toast.LENGTH_SHORT).show();
                    return;
                } else negozio.setVia(via);

                if(civico.equals("")){
                    Toast.makeText(SignUpActivityShop.this,getResources().getString(R.string.numberNotSet),Toast.LENGTH_SHORT).show();
                    return;
                } else negozio.setCivico(civico);

                Intent intent = new Intent(SignUpActivityShop.this, MainActivity.class);
                caricaDati();
                //dopo aver caricato i dati del nuovo negozio e del nuovo utente, si passa alla pagina main
                startActivity(intent);
                finish();
            }
        });
    }

    private void caricaDati() {

        // Crea un mappa con i dati dell'utente
        Map<String, Object> user = new HashMap<>();
        user.put("email",utente.getEmail());
        user.put("sesso", utente.getSesso());
        user.put("data", utente.getDataNascita());
        user.put("immagine",utente.getImmagine());
        user.put("tipo",utente.getTipo());

        // Add a new document with with the current user ID
        fStore.collection("users").document(utente.getIdUtente())
                .set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //inserimento dell'utente nel db completato
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("", "Error adding document", e);
                    }
                });

        // Crea un mappa con i dati del negozio
        Map<String, Object> shop = new HashMap<>();
        shop.put("nome",negozio.getNomeNegozio());
        shop.put("citta",negozio.getCitta());
        shop.put("via",negozio.getVia());
        shop.put("civico",negozio.getCivico());
        shop.put("settore",negozio.getSettore());
        shop.put("descrizione",negozio.getDescrizione());
        shop.put("immagine",negozio.getImmagine());

        fStore.collection("shops").document(negozio.getIdNegozio())
                .set(shop)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //i dati sono stati caricati nel db
                        Toast.makeText(SignUpActivityShop.this,getString(R.string.regComplete),Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("", "Error adding document", e);
                    }
                });
    }

    /**
     * Metodo che gestisce la risposta dell'utente alla richiesta del permesso di accesso all'archivio
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //se abbiamo il permesso di accedere allo storage si può procedere con l'intent
            //per la scelta dell'immagine da caricare
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, PICK_IMAGE);
        }
    }


    private void updateSettore(String toString) {
        negozio.setSettore(toString);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && null != data) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();

                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                decodeFile(picturePath);

                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRef = storage.getReference();
                //salvo il percorso dell'immagine del negozio scelta
                negozio.setImmagine("images/"+ selectedImage.getLastPathSegment());
                StorageReference riversRef = storageRef.child(negozio.getImmagine());
                immagineNonCaricata = false;

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                UploadTask uploadTask = riversRef.putFile(selectedImage);

                // Register observers to listen for when the download is done or if it fails
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.e("upload:","failed");
                        // Handle unsuccessful uploads
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Log.e("upload:","success");
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                        // ...
                    }
                });
            }

        }
    }

    public void decodeFile(String filePath) {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        bitmap = BitmapFactory.decodeFile(filePath, o2);

        image.setImageBitmap(bitmap);
    }

    @Override
    public void onBackPressed() {
        //da questo punto non si torna indietro
        //una possibile soluzione potrebbe essere forzare il logout
    }
}
