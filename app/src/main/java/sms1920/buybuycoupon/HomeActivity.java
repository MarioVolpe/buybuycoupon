package sms1920.buybuycoupon;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import com.bumptech.glide.Glide;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import android.view.MenuItem;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import sms1920.buybuycoupon.model.Coupon;
import sms1920.buybuycoupon.model.Negozio;
import sms1920.buybuycoupon.ui.HomeFragment;

/**
 * Activity che gestisce la home, controlla e recupera gli elementi da visualizzare
 */
public class HomeActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private String status="logged";
    private FirebaseFirestore fStore;
    private FirebaseStorage storage;
    private ImageView image;
    private String path="nothing";
    private static final int RC_SIGN_IN = 123;
    private String tipo = "";
    private ArrayList<Negozio> negozi; //lista dei negozi da mostrare nella recyclerview
    private static Negozio negozio; //dati del negozio dell'utente attualmente connesso
    private static ArrayList<Coupon> coupons; //coupon del negoziante connesso
    private static ArrayList<Coupon> usedCoupons; //coupon usati dall'utente connesso
    private FirebaseUser firebaseUser;
    private static NegoziAdapter negoziAdapter;
    private static String citta;

    public static String getCitta() {
        return citta;
    }

    public static void setCitta(String citta) {
        HomeActivity.citta = citta;
    }

    public static ArrayList<Coupon> getUsedCoupons() {
        return usedCoupons;
    }

    public static void setUsedCoupons(ArrayList<Coupon> usedCoupons) {
        HomeActivity.usedCoupons = usedCoupons;
    }

    public static void setNegozio(Negozio negozio) {
        HomeActivity.negozio = negozio;
    }

    public static void setCoupons(ArrayList<Coupon> coupons) {
        HomeActivity.coupons = coupons;
    }

    public static Negozio getNegozio() {
        return negozio;
    }

    public static ArrayList<Coupon> getCoupons() {
        return coupons;
    }

    public static NegoziAdapter getNegoziAdapter() {
        return negoziAdapter;
    }

    public void setNegoziAdapter(NegoziAdapter negoziAdapter) {
        this.negoziAdapter = negoziAdapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FirebaseAuth fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        firebaseUser = fAuth.getCurrentUser();
        String nome = "";

        if ( firebaseUser != null) {
            // c'è una sessione in corso
            nome = fAuth.getCurrentUser().getDisplayName();
            //prendo il nome dell'utente corrente
            status="logged";
        } else {
            // nessuna sessione in corso
            status="guest";
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        final NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_profile, R.id.nav_shop,
                R.id.nav_tools, R.id.nav_history)
                .setDrawerLayout(drawer)
                .build();


        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        View header = navigationView.getHeaderView(0);
        TextView username = header.findViewById(R.id.nomeCognomeUtente);
        image = header.findViewById(R.id.imageView);
        //recupero gli elementi del layout dall'header del menu sinistro per
        //poi impostarli con l'immagine del profilo e il nome dell'utente

        setCitta(getIntent().getExtras().getString("citta"));

        if(savedInstanceState == null) {//se questa activity era già stata chiamata, allora abbiamo gli arraylist nel bundle
            negozi = getIntent().getParcelableArrayListExtra("negozi");
            if(firebaseUser != null){
                setNegozio((Negozio) getIntent().getExtras().get("negozio"));
                setCoupons(getIntent().getExtras().<Coupon>getParcelableArrayList("coupon"));
                setUsedCoupons(getIntent().getExtras().<Coupon>getParcelableArrayList("usedCoupon"));
            }
        }
        else {
            negozi = savedInstanceState.getParcelableArrayList("negozi");
            if(firebaseUser != null){
                setNegozio((Negozio) savedInstanceState.getParcelable("negozio"));
                setCoupons(savedInstanceState.<Coupon>getParcelableArrayList("coupon"));
                setUsedCoupons(savedInstanceState.<Coupon>getParcelableArrayList("usedCoupon"));
            }
        }

        setNegoziAdapter(new NegoziAdapter(negozi));

        if(status.equals("logged")){
            username.setText(nome);
            //se una sessione è in corso, posso mostrare il nome dell'utente
            navigationView.getMenu().getItem(1).setVisible(true);
            navigationView.getMenu().getItem(3).setVisible(true);
            //inoltre posso mostrare i collegamenti nel menu sinistro per il profilo e lo storico
            navigationView.setCheckedItem(R.id.nav_home);

            final FirebaseUser user = fAuth.getCurrentUser();
            //recupera l'utente che è collegato al momento

            Query users = fStore.collection("users")
                    .whereEqualTo("email", user.getEmail());
            //imposta la query per recuperare i dati dell'utente collegato
            //per recuperare i dati esatti fa un confronto sull'email

            users.get()
                    //esegue la query
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    //fa un forEach per recuperare i documenti uno per volta
                                    //anche se il risultato in questo caso dev'essere un solo
                                    path = document.get("immagine").toString();
                                    tipo = document.get("tipo").toString();
                                }

                                if(tipo.equals("Dealer")||tipo.equals("Negoziante")){
                                    //se l'utente è un negoziante mostro il collegamento
                                    //al negozio nel menu sinistro.
                                    navigationView.getMenu().getItem(2).setVisible(true);
                                    navigationView.setCheckedItem(R.id.nav_home);
                                }
                            }

                            // Create a storage reference from our app
                            StorageReference fileRef = storage.getReferenceFromUrl("gs://bbcoupon-e99f6.appspot.com").child(path);
                            //imposta il percorso dell'immagine del profilo per recuperarla dallo storage remoto

                            if(!path.equals("nothing"))
                                try {
                                    final File localFile = File.createTempFile("images", "jpg");
                                    //crea un file locale temporaneo per salvare l'immagine del profilo

                                    fileRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                        //recupera il file dallo storage
                                        @Override
                                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                            Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                                            Glide.with(HomeActivity.this).load(bitmap).centerCrop().into(image);
                                            //imposta l'immagine nella view del menu laterale sinistro
                                        }
                                    });
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                        }
                    });
        }
    }

    private static MenuItem searchItem;
    //icona della ricerca

    public static MenuItem getSearchItem() {
        return searchItem;
    }

    public static void setSearchItem(MenuItem searchItem) {
        HomeActivity.searchItem = searchItem;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);

        setSearchItem(menu.findItem(R.id.search));
        SearchView searchView = (SearchView) getSearchItem().getActionView();
        //recupera la view per la ricerca

        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //filtra i risultati in tempo reale, in base all'input

                HomeFragment.getNegoziAdapter().getFilter().filter(newText);
                return false;
            }
        });

        MenuItem LoginOrSignup = menu.findItem(R.id.action_login_or_signup);
        //recupera l'elemento nel menu laterale destro, il quale si apre toccando l'omino

        if(status.equals("logged")){
            LoginOrSignup.setTitle(getString(R.string.logout));
            //se l'utente è connesso allora deve apparire la scritta "logout" invece di "accedi o registrati"

            LoginOrSignup.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    AuthUI.getInstance()
                            .signOut(getApplicationContext())
                            //esegue il logout dell'attuale istanza dell'utente
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                public void onComplete(@NonNull Task<Void> task) {
                                    Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                                    //ritorna alla main activity
                                    startActivity(intent);
                                    finish();
                                }
                            });
                    return false;
                }
            });
        } else {

            LoginOrSignup.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    //se l'utente non è loggato l'elemento del menu laterale destro
                    //deve permettere di fare l'accesso o di registrarsi
                    loginOrSignup();
                    return false;
                }
            });
        }

        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void loginOrSignup() {
        final List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build());
        //configura il provider utilizzato per fare l'accesso, ossia e-mail e password

        startActivityForResult(
                //lancia le activity di firebase per fare l'accesso o la registrazione
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setIsSmartLockEnabled(false)
                        .build(),
                RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN && resultCode == RESULT_OK) {

            // Accesso effettuato con successo
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

            alreadyExist(user);
            //controllo se l'utente esiste già
        }
    }

    private void alreadyExist(final FirebaseUser user) {
        fStore = FirebaseFirestore.getInstance();

        Query users = fStore.collection("users")
                .whereEqualTo("email", user.getEmail());
        //prende l'utente che ha la stessa e-mail di quello autenticato dal database

        users.get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (!task.getResult().isEmpty()) {
                                //se trova un risultato allora l'utente già esiste
                                Intent intent = new Intent(HomeActivity.this,MainActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                //altrimenti bisogna registrare l'utente
                                startActivity(new Intent(HomeActivity.this, SignUpActivity.class));
                                finish();
                            }
                        }
                    }
                });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        //salvataggio delle strutture dati
        outState.putParcelableArrayList("negozi",negozi);
        if(firebaseUser != null){
            outState.putParcelable("negozio",getNegozio());
            outState.putParcelableArrayList("coupon",getCoupons());
            outState.putParcelableArrayList("usedCoupon",getUsedCoupons());
        }
    }
}
