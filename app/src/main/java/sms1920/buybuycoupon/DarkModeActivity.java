package sms1920.buybuycoupon;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

public class DarkModeActivity extends AppCompatActivity {

    //costanti per l'uso delle shared preferences
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String DARK_MODE  = "darkMode";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dark_mode);
    }
}
