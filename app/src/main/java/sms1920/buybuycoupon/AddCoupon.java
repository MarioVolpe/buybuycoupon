package sms1920.buybuycoupon;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import sms1920.buybuycoupon.model.Coupon;

public class AddCoupon extends AppCompatActivity {

    private EditText nome;
    private EditText desc;
    private TextView scadenza;
    private EditText percentuale;
    private EditText prezzo;
    private final int[] data = new int[]{0, 0, 0};
    private Coupon coupon;
    private FirebaseFirestore fStore;
    private String chiamante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_coupon);

        //inizializzazione views
        nome = findViewById(R.id.nome_coupon);
        desc = findViewById(R.id.couponDesc);
        scadenza = findViewById(R.id.scadenzaCoupon);
        percentuale = findViewById(R.id.percentuale);
        prezzo = findViewById(R.id.prezzo);
        Button salva = findViewById(R.id.salvaCoupon);
        fStore = FirebaseFirestore.getInstance();

        salva.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        coupon = new Coupon();

        chiamante = getIntent().getStringExtra("chiamante");
        //in base al chiamante si verifica la modifica o l'aggiunta del coupon

        if(chiamante.equals("edit")) {
            //variante dell'activity: versione modifica coupon
            //recupera i dati del coupon e inizializza le views
            //con i dati di tale coupon
            coupon = getIntent().getParcelableExtra("coupon");
            salva.setText(getString(R.string.save));
            prezzo.setText(coupon.getPrezzo());
            percentuale.setText(coupon.getPercentuale());
            nome.setText(coupon.getNome());
            desc.setText(coupon.getDescrizione());
            scadenza.setText(coupon.getScadenza());
            //se la data non viene cambiata non importa
            data[0]=1;
        }


        final Calendar calendario = Calendar.getInstance();

        scadenza.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int giorno = calendario.get(Calendar.DAY_OF_MONTH);
                int mese = calendario.get(Calendar.MONTH);
                int anno = calendario.get(Calendar.YEAR);

                final DatePickerDialog datePickerDialog;

                //scelta della data di scadenza del coupon
                datePickerDialog = new DatePickerDialog(AddCoupon.this, 0, new DatePickerDialog.OnDateSetListener()
                {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        scadenza.setText(dayOfMonth + " / " + (month + 1) + " / " + year);
                        data[0]=dayOfMonth;
                        data[1]=month+1;
                        data[2]=year;
                        coupon.setScadenza(""+data[0]+"/"+data[1]+"/"+data[2]);
                    }
                }, anno, mese, giorno);

                datePickerDialog.show();
            }
        });

        salva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(prezzo.getText().toString().equals("")||percentuale.getText().toString().equals("")||nome.getText().toString().equals("")
                    ||desc.getText().toString().equals("")||data[0]==0){
                    //controllo che ogni campo sia stato inserito e non sia vuoto
                    Toast.makeText(AddCoupon.this,getString(R.string.insert_values),Toast.LENGTH_SHORT).show();
                    return;
                }

                //aggiorno/inizializzo i dati del coupon
                coupon.setPrezzo(prezzo.getText().toString().trim());
                coupon.setPercentuale(percentuale.getText().toString().trim());
                coupon.setNome(nome.getText().toString().trim());
                coupon.setDescrizione(desc.getText().toString().trim());
                coupon.setNegozio(FirebaseAuth.getInstance().getCurrentUser().getUid());

                if(chiamante.equals("edit"))
                    //in caso di modifica chiamo l'update
                    update();
                else//altrimenti il caricamento del nuovo coupon
                caricaDati();
            }
        });
    }

    private void update() { fStore = FirebaseFirestore.getInstance();

        // Crea un mappa con i dati del coupon
        Map<String, Object> coupondb = new HashMap<>();
        coupondb.put("nome", coupon.getNome());
        coupondb.put("descrizione", coupon.getDescrizione());
        coupondb.put("scadenza", coupon.getScadenza());
        coupondb.put("negozio",coupon.getNegozio());
        coupondb.put("prezzo",coupon.getPrezzo());
        coupondb.put("percentuale",coupon.getPercentuale());

        // aggiorna il coupon con i nuovi dati
        fStore.collection("coupons").document(coupon.getIdCoupon())
                .update(coupondb)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //ritorna alla home per aggiornare le views con le modifiche salvate
                        goBack();
                    }
                });
    }

    private void caricaDati() {
        fStore = FirebaseFirestore.getInstance();

        // Crea un mappa con i dati del coupon
        Map<String, Object> coupondb = new HashMap<>();
        coupondb.put("nome", coupon.getNome());
        coupondb.put("descrizione", coupon.getDescrizione());
        coupondb.put("scadenza", coupon.getScadenza());
        coupondb.put("negozio",coupon.getNegozio());
        coupondb.put("prezzo",coupon.getPrezzo());
        coupondb.put("percentuale",coupon.getPercentuale());

        // aggiunge il nuovo coupon al db
        fStore.collection("coupons")
                .add(coupondb)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        //ritorna alla home per aggiornare le views con le modifiche salvate
                        goBack();
                    }
                });
    }

    //ritorna alla home dopo le modifiche
    private void goBack() {
        Intent intent = new Intent(AddCoupon.this,SplashActivity.class);
        intent.putExtra("destinazione","home");
        intent.putExtra("citta",getIntent().getExtras().getString("citta"));
        startActivity(intent);
        super.finish();
        finish();
    }
}