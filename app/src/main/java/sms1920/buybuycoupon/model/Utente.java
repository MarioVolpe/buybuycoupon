package sms1920.buybuycoupon.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Classe model dell'utente che rappresenta esattamente ciò che contiene il Database
 * riguardo tutti coloro che si registrano come semplici clienti.
 * In particolare il campo "immagine" contiene il percorso e il nome dell'immagine nello storage remoto.
 * Il campo tipo esprime il tipo di utente, il quale può essere cliente o negoziante.
 */
public class Utente implements Parcelable {
    private String idUtente;
    private String nome;
    private String cognome;
    private String dataNascita;
    private String sesso;
    private String email;
    private String password;
    private String immagine;
    private String tipo;


    protected Utente(Parcel in) {
        idUtente = in.readString();
        nome = in.readString();
        cognome = in.readString();
        dataNascita = in.readString();
        sesso = in.readString();
        email = in.readString();
        password = in.readString();
        immagine = in.readString();
        tipo = in.readString();
    }

    public static final Creator<Utente> CREATOR = new Creator<Utente>() {
        @Override
        public Utente createFromParcel(Parcel in) {
            return new Utente(in);
        }

        @Override
        public Utente[] newArray(int size) {
            return new Utente[size];
        }
    };

    public Utente() {

    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getImmagine() {
        return immagine;
    }

    public void setImmagine(String immagine) {
        this.immagine = immagine;
    }

    public String getIdUtente() {
        return idUtente;
    }

    public void setIdUtente(String idUtente) {
        this.idUtente = idUtente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(String dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(idUtente);
        parcel.writeString(nome);
        parcel.writeString(cognome);
        parcel.writeString(dataNascita);
        parcel.writeString(sesso);
        parcel.writeString(email);
        parcel.writeString(password);
        parcel.writeString(immagine);
        parcel.writeString(tipo);
    }
}
