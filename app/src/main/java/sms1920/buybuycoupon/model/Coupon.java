package sms1920.buybuycoupon.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Classe model del coupon che rappresenta esattamente ciò che contiene il Database
 * riguardo tutti i coupon che vengono aggiunti dai negozianti.
 * In particolare il campo "negozio" contiene l'id del negozio che ha emesso il coupon.
 */
public class Coupon implements Parcelable {
    public Coupon(){}

    private String idCoupon;
    private String nome;
    private String scadenza;
    private String percentuale;
    private String prezzo;
    private String descrizione;
    private String negozio;


    public static final Creator<Coupon> CREATOR = new Creator<Coupon>() {
        @Override
        public Coupon createFromParcel(Parcel in) {
            return new Coupon(in);
        }

        @Override
        public Coupon[] newArray(int size) {
            return new Coupon[size];
        }
    };



    public String getNegozio() {
        return negozio;
    }

    public void setNegozio(String negozio) {
        this.negozio = negozio;
    }

    protected Coupon(Parcel in) {
        idCoupon = in.readString();
        nome = in.readString();
        scadenza = in.readString();
        percentuale = in.readString();
        prezzo = in.readString();
        descrizione = in.readString();
        negozio = in.readString();
    }

    public String getIdCoupon() {
        return idCoupon;
    }

    public void setIdCoupon(String idCoupon) {
        this.idCoupon = idCoupon;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getScadenza() {
        return scadenza;
    }

    public void setScadenza(String scadenza) {
        this.scadenza = scadenza;
    }

    public String getPercentuale() {
        return percentuale;
    }

    public void setPercentuale(String percentuale) {
        this.percentuale = percentuale;
    }

    public String getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(String prezzo) {
        this.prezzo = prezzo;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(idCoupon);
        parcel.writeString(nome);
        parcel.writeString(scadenza);
        parcel.writeString(percentuale);
        parcel.writeString(prezzo);
        parcel.writeString(descrizione);
        parcel.writeString(negozio);
    }
}