package sms1920.buybuycoupon.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Classe model del negozio che rappresenta esattamente ciò che contiene il Database
 * riguardo tutti i negozi che vengono aggiunti dai negozianti in fase di registrazione.
 * In particolare il campo "immagine" contiene il percorso e il nome dell'immagine nello storage remoto.
 * L'id del negozio corrisponde all'id dell'utente a cui appartiene.
 */

public class Negozio implements Parcelable {
    private String idNegozio;
    private String nomeNegozio;
    private String descrizione;
    private String settore;
    private String via;
    private String civico;
    private String citta;
    private String immagine;

    public Negozio() {
    }

    protected Negozio(Parcel in) {
        idNegozio = in.readString();
        nomeNegozio = in.readString();
        descrizione = in.readString();
        settore = in.readString();
        via = in.readString();
        civico = in.readString();
        citta = in.readString();
        immagine = in.readString();
    }

    public static final Creator<Negozio> CREATOR = new Creator<Negozio>() {
        @Override
        public Negozio createFromParcel(Parcel in) {
            return new Negozio(in);
        }

        @Override
        public Negozio[] newArray(int size) {
            return new Negozio[size];
        }
    };

    public String getImmagine() {
        return immagine;
    }

    public void setImmagine(String immagine) {
        this.immagine = immagine;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getIdNegozio() {
        return idNegozio;
    }

    public void setIdNegozio(String idNegozio) {
        this.idNegozio = idNegozio;
    }

    public String getNomeNegozio() {
        return nomeNegozio;
    }

    public void setNomeNegozio(String nomeNegozio) {
        this.nomeNegozio = nomeNegozio;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getSettore() {
        return settore;
    }

    public void setSettore(String settore) {
        this.settore = settore;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getCivico() {
        return civico;
    }

    public void setCivico(String civico) {
        this.civico = civico;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(idNegozio);
        parcel.writeString(nomeNegozio);
        parcel.writeString(descrizione);
        parcel.writeString(settore);
        parcel.writeString(via);
        parcel.writeString(civico);
        parcel.writeString(citta);
        parcel.writeString(immagine);
    }
}
