package sms1920.buybuycoupon;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Activity principale dell'applicazione, contiene la logica per il gps
 */
public class MainActivity extends AppCompatActivity implements LocationListener {

    private AutoCompleteTextView citta;
    private final static int FINE_LOCATION_ID = 666;
    //variabile di controllo per agire in caso di permesso concesso o meno
    private boolean permessoConcesso = false;
    private LocationManager locationManager;
    private double longitude=200;
    private double latitude=100;
    private static final int RC_SIGN_IN = 123;
    private FirebaseAuth fAuth;
    private FirebaseFirestore fStore;
    private boolean darkMode = false;
    private String[] countries;
    private String cityName="";
    private Button LoginOrSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //lettura delle shared preferences per sapere se impostare il tema scuro o no
        try {
            darkMode = loadData();
        } catch(Exception e) {
            e.printStackTrace();
        }

        //attivazione del tema scuro sulla base delle shared preferences
        if(darkMode) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }

        countries = getResources().getStringArray(R.array.citta);
        //array costruito con le città di tutta Italia, presenti nelle res
        //l'adapter serve per inizializzare l'autocompleteTextView, la quale mostrerà le città suggerite in base all'input della tastiera

        //inizializza l'autocompleteTextView con i dati presenti nell'array delle città nelle risorse
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, countries);
        citta = findViewById(R.id.citta);
        citta.setAdapter(adapter);

        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        //verifica del permesso di localizzazione
        if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},FINE_LOCATION_ID);
        else {
            permessoConcesso = true;
        }

        Button avanti = findViewById(R.id.avanti);
        avanti.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        avanti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cittaTXT = citta.getText().toString().trim();
                //se è stata inserita la città o è stata rilevata dal gps
                if(cittaTXT.length()>0) {
                    Intent intent = new Intent(MainActivity.this, SplashActivity.class);
                    intent.putExtra("citta",citta.getText().toString().trim());

                    if(fAuth.getCurrentUser() != null)
                        intent.putExtra("destinazione","home");
                    else intent.putExtra("destinazione","homeGuest");

                    startActivity(intent);
                }
                else
                    Toast.makeText(MainActivity.this,getString(R.string.insert_town_before),Toast.LENGTH_LONG).show();
            }
        });

        LoginOrSignup = findViewById(R.id.LoginOrSignup);
        LoginOrSignup.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        if(fAuth.getCurrentUser() != null) {
            // se l'utente è già connesso allora cambiamo il bottone per l'accesso/registrazione in logout.
            LoginOrSignup.setText(getString(R.string.logout));
        }

        //listener per lanciare l'activity di accesso/registrazione o logout
        LoginOrSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(fAuth.getCurrentUser()!=null)
                    //se l'utente è connesso, allora al tocco del bottone fa il logout
                    AuthUI.getInstance()
                            .signOut(getApplicationContext())
                            //esegue il logout dell'attuale istanza dell'utente
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                public void onComplete(@NonNull Task<Void> task) {
                                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                                    // aggiorna la main activity
                                    startActivity(intent);
                                    finish();
                                }
                            });
                else loginOrSignup();
            }
        });
    }

    private boolean loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences(DarkModeActivity.SHARED_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(DarkModeActivity.DARK_MODE, false);
    }

    public void loginOrSignup() {
        final List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build());
        //configura il provider utilizzato per fare l'accesso, ossia e-mail e password

        startActivityForResult(
                //lancia le activity di firebase per fare l'accesso o la registrazione
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setIsSmartLockEnabled(false)
                        .build(),
                RC_SIGN_IN);
    }

    @Override
    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {}

    @Override
    public void onProviderEnabled(String s) {
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.gps_enabled),
                Toast.LENGTH_SHORT).show();
        if(permessoConcesso)
            setLocation();
    }

    @Override
    public void onProviderDisabled(String s) {
        Snackbar.make(findViewById(R.id.MainActivityConstr), getResources().getString(R.string.gps_notenabled),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getResources().getString(R.string.enable), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(i);
                    }
                })
                .show();
        if(permessoConcesso)
            setLocation();
    }

    /**
     * Questo metodo utilizza una libreria che recupera il nome della città in base alle coordinate
     */
    @SuppressLint("MissingPermission")
    private void setLocation() {
        onLocationChanged(locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));

        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        cityName = addresses.get(0).getAddressLine(0);

        for(String citta:countries)
            if(cityName.contains(citta)){
                cityName = citta;
                break;
            }

        citta.setText(cityName);
        citta.refreshDrawableState();
    }

    /**
     * Metodo che gestisce la risposta dell'utente alla richiesta del permesso di utilizzo del GPS
     */
    @Override
    @SuppressLint("MissingPermission")
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == FINE_LOCATION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                permessoConcesso = true;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (permessoConcesso) {
            //controllo che il GPS sia attivo solo se il permesso di utilizzo è stato concesso
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Snackbar.make(findViewById(R.id.MainActivityConstr), getString(R.string.gps_notenabled),
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.enable), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(i);
                            }
                        })
                        .show();
            }
        }

        if(permessoConcesso)
            if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                //se ho il permesso e il gps è attivato, allora prendo la posizione
                setLocation();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN && resultCode == RESULT_OK) {

            // Accesso effettuato con successo
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

            alreadyExist(user);
            // controllo se l'utente esiste già
        }
    }

    private void alreadyExist(final FirebaseUser user) {
        fStore = FirebaseFirestore.getInstance();

        Query users = fStore.collection("users")
                .whereEqualTo("email", user.getEmail());
        //prende l'utente che ha la stessa e-mail di quello autenticato dal database

        users.get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (!task.getResult().isEmpty()) {
                                //se trova un risultato allora l'utente già esiste
                                Intent intent = new Intent(MainActivity.this,MainActivity.class);
                                intent.putExtra("destinazione","homeLogin");
                                startActivity(intent);
                                finish();
                            } else {
                                //altrimenti deve registrarsi
                                Intent intent = new Intent(MainActivity.this,SignUpActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    }
                });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(fAuth.getCurrentUser() != null) {
            // se l'utente è già connesso allora cambiamo il bottone per l'accesso/registrazione in logout.
            LoginOrSignup.setText(getString(R.string.logout));
        }
    }

    @Override
    public void onBackPressed() {
        final Dialog mDialog = new Dialog(MainActivity.this);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.quit_dialog);
        mDialog.setTitle(R.string.quitDialogTitle);

        Button pos = mDialog.findViewById(R.id.pos);
        Button neg = mDialog.findViewById(R.id.neg);

        pos.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        pos.setTextSize(13);
        neg.setBackgroundColor(getResources().getColor(R.color.RED));
        neg.setTextSize(13);

        pos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        neg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //chiude semplicemente il dialog
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }
}