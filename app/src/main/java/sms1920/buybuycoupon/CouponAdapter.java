package sms1920.buybuycoupon;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import sms1920.buybuycoupon.model.Coupon;

/**
 * Adapter con i dati relativi ai coupon
 */
public class CouponAdapter extends RecyclerView.Adapter<CouponAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Coupon> coupons;
    private String chiamante;

    public CouponAdapter(ArrayList<Coupon> c,String chiamante){
        if(c!=null)
            //se non sono stati caricati in tempo, evito il crash con il nullPointer
           this.coupons=c;
        else this.coupons = new ArrayList<>();
        this.chiamante=chiamante;
    }

    @NonNull
    @Override
    public CouponAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if(chiamante.equals("edit"))
            //la card della recyclerView varia in base alla chiamata, infatti in modalità edit c'è il bottone "modifica" in più
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_coupon_shop,parent,false);
        else view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_coupon,parent,false);

        ViewHolder viewHolder = new ViewHolder(view);
        context = parent.getContext();
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final CouponAdapter.ViewHolder holder, final int position) {

        if(coupons.size()==0)
            //gestisco il caso in cui non ci siano coupon da mostrare
            if(chiamante.equals("storico"))
                holder.subtitleCardTextView.setText(context.getResources().getString(R.string.no_coupon_used));
            else holder.subtitleCardTextView.setText(context.getResources().getString(R.string.no_coupon));
        else {
            //recupero un coupon per volta
            Coupon c = coupons.get(position);
            //e mostro i dati relativi ad esso
            if(chiamante.equals("storico"))
                holder.titleCardTextView.setText(c.getNome() + " | " + context.getString(R.string.requestDate)+":"+c.getScadenza());
            else holder.titleCardTextView.setText(c.getNome() + " | " + context.getString(R.string.expirationDate)+": "+c.getScadenza());
            holder.subtitleCardTextView.setText(c.getPercentuale() + "% OFF | "+context.getString(R.string.price)+": " + c.getPrezzo() + "€");
            holder.textCardTextView.setText(c.getDescrizione());
        }
    }

    @Override
    public int getItemCount() {
        if(coupons.size()==0)
            //anche in caso di array vuoto si vede una card
            return 1;
        return coupons.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleCardTextView;
        TextView subtitleCardTextView;
        TextView textCardTextView;
        Button edit;
        CardView cardCoupon;

        ViewHolder(final View itemView)
        {
            super(itemView);

            if(chiamante.equals("edit")){
                //inizializzazione card modalità modifica
                cardCoupon = itemView.findViewById(R.id.couponCardShop);
                titleCardTextView = itemView.findViewById(R.id.titleCardTextViews);
                subtitleCardTextView = itemView.findViewById(R.id.subtitleCardTextViews);
                textCardTextView = itemView.findViewById(R.id.textCardTextViews);
                edit = itemView.findViewById(R.id.editCoupon);

                if(coupons.size()==0)
                    //se non ci sono coupon inutile mostrare il pulsante di modifica
                    edit.setVisibility(View.INVISIBLE);

                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //chiamata all'activity di modifica del coupon
                        Intent intent = new Intent(context,AddCoupon.class);
                        intent.putExtra("chiamante","edit");
                        intent.putExtra("citta",HomeActivity.getCitta());
                        intent.putExtra("coupon",coupons.get(getAdapterPosition()));
                        ActivityCompat.startActivity(context, intent, null);
                        try {
                            finalize();
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    }
                });
            } else {
                //inizializzazione card modalità storico o cliente
                cardCoupon = itemView.findViewById(R.id.couponCard);
                titleCardTextView = itemView.findViewById(R.id.titleCardTextViewc);
                subtitleCardTextView = itemView.findViewById(R.id.subtitleCardTextViewc);
                textCardTextView = itemView.findViewById(R.id.textCardTextViewc);
            }

            if(coupons.size()==0)
                //se non ci sono coupon, la card non dev'essere cliccabile
                itemView.setClickable(false);
            else {
                if(chiamante.equals("storico"))
                    //in modalità storico la card non dev'essere cliaccabile
                    itemView.setClickable(false);
                else {

                    if(FirebaseAuth.getInstance().getCurrentUser()==null){
                        //se siamo in modalità ospite, il coupon non può essere richiesto
                        //dunque al tocco della card, l'ospite viene avvisato
                        itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Snackbar.make(cardCoupon, context.getString(R.string.do_login),
                                        Snackbar.LENGTH_INDEFINITE)
                                        .setAction(context.getString(R.string.login), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                Intent i = new Intent(context,MainActivity.class);
                                                context.startActivity(i);
                                                try {
                                                    finalize();
                                                } catch (Throwable throwable) {
                                                    throwable.printStackTrace();
                                                }
                                            }
                                        })
                                        .show();
                            }
                        });
                    } else {

                        itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //gestione del tocco della card
                                Intent intent = new Intent(context, ShareCouponActivity.class);

                                if (chiamante.equals("edit"))
                                    //la vista della shareCouponActivity varia in base al chiamante
                                    intent.putExtra("chiamante", "negoziante");
                                else {
                                    intent.putExtra("chiamante", "cliente");
                                    intent.putExtra("idUtente", FirebaseAuth.getInstance().getCurrentUser().getUid());
                                }

                                intent.putExtra("coupon", coupons.get(getAdapterPosition()));
                                intent.putExtra("citta",NegozioActivity.getCitta());
                                //passaggio alla shareCouponActivity del coupon selezionato
                                ActivityCompat.startActivity(context, intent, null);
                                try {
                                    finalize();
                                } catch (Throwable throwable) {
                                    throwable.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        }
    }

}
