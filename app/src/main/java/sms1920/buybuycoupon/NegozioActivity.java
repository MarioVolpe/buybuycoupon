package sms1920.buybuycoupon;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import sms1920.buybuycoupon.model.Coupon;
import sms1920.buybuycoupon.model.Negozio;

public class NegozioActivity extends AppCompatActivity {

    private ImageView imageView;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private static String citta;

    public static String getCitta() {
        return citta;
    }

    public static void setCitta(String citta) {
        NegozioActivity.citta = citta;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_negozio);

        imageView = findViewById(R.id.imgNegozio);
        TextView nome = findViewById(R.id.nomeNegozio);
        TextView descrizione = findViewById(R.id.descrizioneNegozio);
        TextView apriMap = findViewById(R.id.descrizioneMapButton);
        RecyclerView coupon = findViewById(R.id.coupon);

        citta = getIntent().getExtras().getString("citta");

        //prende dall'activity chiamante la lista dei negozi e l'id del negozio
        //di cui mostrare i dettagli.
        Negozio negozio = (Negozio) getIntent().getExtras().get("negozio");
        ArrayList<Coupon> coupons = getIntent().getExtras().getParcelableArrayList("coupon");

        nome.setText(negozio.getNomeNegozio());
        descrizione.setText(negozio.getDescrizione());

        StorageReference fileRef = storage.getReferenceFromUrl("gs://bbcoupon-e99f6.appspot.com").child(negozio.getImmagine());
        //imposta il percorso dell'immagine del negozio per recuperarla dallo storage remoto

        try {
            final File localFile = File.createTempFile("images", "jpg");
            //crea un file locale temporaneo per salvare l'immagine del negozio

            fileRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    Glide.with(NegozioActivity.this).load(bitmap).centerCrop().into(imageView);
                    //imposta l'immagine nell'imageview che sta in alto.
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

        //inizializzazione recyclerView contenente i coupon offerti dal negozio
        CouponAdapter couponAdapter = new CouponAdapter(coupons, "");
        coupon.setLayoutManager(new LinearLayoutManager(this));
        coupon.setItemAnimator(new DefaultItemAnimator());
        coupon.setAdapter(couponAdapter);

        final String indirrizzo = ""+negozio.getVia()+" "+negozio.getCivico();

        ImageButton mapButtn = findViewById(R.id.mapButton);

        //sia l'icona che la scritta lanciano l'intent implicito per
        //l'apertura della mappa che porta al negozio
        mapButtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMap(indirrizzo);
            }
        });

        apriMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMap(indirrizzo);
            }
        });
    }

    private void openMap(String indirrizzo) {
        //intent implicito che ricerca un servizio per portare il cliente all'indirizzo
        //del negozio, il quale viene passato come parametro
        Uri mapsIntentUri = Uri.parse("geo:0,0?q="+indirrizzo);
        Intent mapsIntent = new Intent(Intent.ACTION_VIEW, mapsIntentUri);
        mapsIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapsIntent);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
