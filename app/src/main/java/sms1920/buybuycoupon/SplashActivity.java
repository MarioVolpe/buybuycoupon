package sms1920.buybuycoupon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;

import sms1920.buybuycoupon.model.Coupon;
import sms1920.buybuycoupon.model.Negozio;

public class SplashActivity extends Activity {
    private final int SPLASH_TIME_OUT = 5000; //Costante che definisce la durata della splash (5 sec)
    private Intent i;
    private ArrayList<Negozio> negozi; //array di negozi da mostrare nella home
    private ArrayList<Coupon> coupons; //array di coupon del negoziante connesso
    private ArrayList<Coupon> usedCoupons; //array di coupon usati
    private Negozio n;
    private String destinazione;
    private String idNegozio;
    private int position=0;
    private int j=0;
    private FirebaseUser firebaseUser;
    private String cittaCheck;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        destinazione = getIntent().getExtras().getString("destinazione"); //riprende l'activity che deve essere lanciata dopo la splash

        //inserimento del logo nella splash
        ImageView logo = findViewById(R.id.imageViewLogo);
        Glide.with(this).load(R.mipmap.ico_niente_ombra).centerCrop().into(logo);

        //definizione delle varie activity che dovranno essere lanciate dalla splash
        //e degli extra da passare
        switch(destinazione) {
            case "homeLogin":
                i = new Intent(SplashActivity.this, MainActivity.class);
                break;
            case "home":
                i = new Intent(SplashActivity.this, HomeActivity.class);
                break;
            case "homeGuest":
                i = new Intent(SplashActivity.this, HomeActivity.class);
                i.putExtra("citta",getIntent().getExtras().getString("citta"));
                break;
            case "coupon":
                i = new Intent(SplashActivity.this, NegozioActivity.class);
                n = (Negozio) getIntent().getExtras().get("negozio");
                idNegozio = n.getIdNegozio();
                i.putExtra("negozio",n);
                break;
        }

        cittaCheck = getIntent().getExtras().getString("citta");

        if(!destinazione.equals("coupon")) {
            if(!destinazione.equals("homeGuest"))
                //se non è un ospite recupero l'id del possibile negozio dell'utente
                idNegozio = FirebaseAuth.getInstance().getCurrentUser().getUid();

            Query shops = FirebaseFirestore.getInstance().collection("shops");
            //inizializza la query puntando alla "tabella" dei negozi

            negozi = new ArrayList<>();

            shops.get()
                    //prende tutti i negozi dal database
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                position = 0;

                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    //recupera le informazioni sull'indirizzo del negozio
                                    String cittaEl = document.get("citta").toString();

                                    //controllo che il negozio sia nella stessa città indicata dall'utente
                                    if (cittaEl.equals(cittaCheck)) {
                                        if(firebaseUser!=null) {
                                            //se non siamo in modalità ospite...
                                            if (firebaseUser.getUid().equals(document.getId()))
                                                //...e il negozio è di proprietà dell'utente connesso,
                                                //allora il negozio estratto dal db è quello da visualizzare nella negozioActivity
                                                inizializzaMioNegozio(document);

                                                //in caso contrario aggiungiamo il negozio alla lista dei negozi da visualizzare nella home
                                            else inizializzaNegozi(document);

                                        } else inizializzaNegozi(document);


                                    } else if (firebaseUser!=null&&firebaseUser.getUid().equals(document.getId()))
                                        //se il negozio è di proprietà dell'utente connesso,
                                        //allora il negozio estratto dal db è quello da visualizzare nella negozioActivity
                                        inizializzaMioNegozio(document);
                                }

                                i.putParcelableArrayListExtra("negozi", negozi);
                            }
                        }
                    });
        }

        if(!destinazione.equals("homeGuest")) {
            //prende i coupon dal db
            recuperaCoupon();
            if(firebaseUser!=null)
                //inizializza lo storico
                couponUsati();
        }

        i.putExtra("citta", cittaCheck);

        //esecuzione della splash con durata definita dalla costante "SPLASH_TIME_OUT"
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    private void inizializzaMioNegozio(QueryDocumentSnapshot document) {
        n = new Negozio();
        n.setImmagine(document.get("immagine").toString());
        n.setNomeNegozio(document.get("nome").toString());
        n.setVia(document.get("via").toString());
        n.setCitta(document.get("citta").toString());
        n.setCivico(document.get("civico").toString());
        n.setSettore(document.get("settore").toString());
        n.setDescrizione(document.get("descrizione").toString());
        n.setIdNegozio(document.getId());
        i.putExtra("negozio",n);
    }

    private ArrayList<Coupon> u;

    /**
     * Per lo storico: recupera i dati dei coupon utilizzati
     */
    private void couponUsati() {

        usedCoupons = new ArrayList<>();
        u = new ArrayList<>();

        //prende i coupon usati dall'utente connesso in questo momento
        Query couponUsed = FirebaseFirestore.getInstance().collection("activeCoupons").whereEqualTo("cliente",firebaseUser.getUid());

        couponUsed.get().
                addOnCompleteListener(
                        new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> taskUsati) {
                                if (taskUsati.isSuccessful()) {
                                    int m = 0;
                                    for (DocumentSnapshot documentSnapshot : taskUsati.getResult()) {
                                        //recupera i dati dei coupon usati
                                        u.add(new Coupon());
                                        u.get(m).setIdCoupon(documentSnapshot.getString("coupon"));
                                        u.get(m).setNome(documentSnapshot.getString("cliente"));
                                        u.get(m).setScadenza(documentSnapshot.getString("data"));
                                        m++;
                                    }

                                    ArrayList<String> ids;
                                    if (u.size() > 0) {
                                        //se ci sono coupon usati da questo utente,
                                        //allora salvo gli id dei coupon
                                        ids = new ArrayList<>();
                                        for (Coupon coupon : u)
                                            ids.add(coupon.getIdCoupon());

                                        //grazie agli id dei coupon usati recuperati, posso fare una
                                        //query direttamente per recuperare i dati solo dei coupon usati
                                        Query query = FirebaseFirestore.getInstance().collection("coupons").
                                                whereIn(FieldPath.documentId(), ids);

                                        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                int j = 0;
                                                if (task.isSuccessful()) {
                                                    for (DocumentSnapshot document : task.getResult()) {
                                                        usedCoupons.add(new Coupon());
                                                        //aggiunge all'arrayList un coupon e lo inizializza con i dati recuperati dal database
                                                        usedCoupons.get(j).setIdCoupon(document.getId());
                                                        usedCoupons.get(j).setPercentuale(document.get("percentuale").toString());
                                                        usedCoupons.get(j).setNome(document.get("nome").toString());
                                                        usedCoupons.get(j).setPrezzo(document.get("prezzo").toString());
                                                        usedCoupons.get(j).setScadenza(document.get("scadenza").toString());
                                                        usedCoupons.get(j).setDescrizione(document.get("descrizione").toString());
                                                        usedCoupons.get(j).setNegozio(document.get("negozio").toString());
                                                        j++;
                                                    }
                                                    i.putParcelableArrayListExtra("usedCoupon", usedCoupons);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                );
    }

    private void recuperaCoupon() {
        //prende i coupon del negozio indicato nella variabile idNegozio
        Query query = FirebaseFirestore.getInstance().collection("coupons").
                whereEqualTo("negozio", idNegozio);
        //inizializza la query puntando alla "tabella" dei coupon

        coupons = new ArrayList<>();

        query.get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            int j = 0;
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                //controllo prima la scadenza del coupon
                                if(!scaduto(document.get("scadenza").toString())) {

                                    if(destinazione.equals("coupon")&&firebaseUser!=null) {
                                        //controllo se il coupon è già stato utilizzato
                                        //in caso positivo non lo mostro tra quelli disponibili
                                        giaUsato(document);

                                    } else {
                                        coupons.add(new Coupon());
                                        //aggiunge all'arrayList un coupon e lo inizializza con i dati recuperati dal database
                                        coupons.get(j).setIdCoupon(document.getId());
                                        coupons.get(j).setPercentuale(document.get("percentuale").toString());
                                        coupons.get(j).setNome(document.get("nome").toString());
                                        coupons.get(j).setPrezzo(document.get("prezzo").toString());
                                        coupons.get(j).setScadenza(document.get("scadenza").toString());
                                        coupons.get(j).setDescrizione(document.get("descrizione").toString());
                                        coupons.get(j).setNegozio(document.get("negozio").toString());
                                        j++;
                                    }
                                }
                            }
                            i.putParcelableArrayListExtra("coupon", coupons);
                            i.putExtra("negozio", n);
                        }
                    }
                });
    }

    private void giaUsato(final QueryDocumentSnapshot document) {
        final String idUtente = firebaseUser.getUid();

        Query activeCoupons = FirebaseFirestore.getInstance().collection("activeCoupons").
                whereEqualTo("coupon",document.getId()).whereEqualTo("cliente",idUtente);
        //inizializza la query puntando alla "tabella" dei coupon già usati

        activeCoupons.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()) {
                    for (QueryDocumentSnapshot document2 : task.getResult())
                        if (document2.exists()) {
                            if (document2.getString("coupon").equals(document.getId()) &&
                                    document2.getString("cliente").equals(idUtente)) {
                                //se trova corrispondenza tra l'id del coupon
                                //e il campo "coupon" del coupon già usato e l'ide del cliente
                                //e il campo "cliente" del coupon già usato
                                //allora il coupon è già stato fruito da questo specifico utente
                                return;
                            }
                        }
                    //incaso contrario aggiunge il coupon alla lista dei coupon disponibili
                    coupons.add(new Coupon());
                    //aggiunge all'arrayList un coupon e lo inizializza con i dati recuperati dal database
                    coupons.get(j).setIdCoupon(document.getId());
                    coupons.get(j).setPercentuale(document.get("percentuale").toString());
                    coupons.get(j).setNome(document.get("nome").toString());
                    coupons.get(j).setPrezzo(document.get("prezzo").toString());
                    coupons.get(j).setScadenza(document.get("scadenza").toString());
                    coupons.get(j).setDescrizione(document.get("descrizione").toString());
                    coupons.get(j).setNegozio(document.get("negozio").toString());
                    j++;
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                coupons.add(new Coupon());
                //aggiunge all'arrayList un coupon e lo inizializza con i dati recuperati dal database
                coupons.get(j).setIdCoupon(document.getId());
                coupons.get(j).setPercentuale(document.get("percentuale").toString());
                coupons.get(j).setNome(document.get("nome").toString());
                coupons.get(j).setPrezzo(document.get("prezzo").toString());
                coupons.get(j).setScadenza(document.get("scadenza").toString());
                coupons.get(j).setDescrizione(document.get("descrizione").toString());
                coupons.get(j).setNegozio(document.get("negozio").toString());
                j++;
            }
        });
    }

    /**
     * @param scadenza data di scadenza
     * @return true se il coupon è scaduto, false altrimenti
     */
    private boolean scaduto(String scadenza) {
        Integer[] data = parseData(scadenza);
        Date date = new Date();
        int g=date.getDate();
        int m=date.getMonth()+1;
        int a=date.getYear()+1900;

        if(data[2]<a)
            return true;
        else if (data[2]==a){
            if(data[1]<m)
                return true;
            else if(data[1]==m){
                return data[0] < g;
            }
        }

        return false;
    }

    private void inizializzaNegozi(QueryDocumentSnapshot document) {
        negozi.add(new Negozio());
        //aggiunge all'arrayList un negozio e lo inizializza con i dati recuperati dal database
        negozi.get(position).setImmagine(document.get("immagine").toString());
        negozi.get(position).setNomeNegozio(document.get("nome").toString());
        negozi.get(position).setVia(document.get("via").toString());
        negozi.get(position).setCitta(document.get("citta").toString());
        negozi.get(position).setCivico(document.get("civico").toString());
        negozi.get(position).setSettore(document.get("settore").toString());
        negozi.get(position).setDescrizione(document.get("descrizione").toString());
        negozi.get(position).setIdNegozio(document.getId());
        position++;
    }

    private final static int C = 48;

    public static Integer[] parseData(String data){
        char[] c = data.toCharArray();
        int giorno=c[0]-C;
        int mese=0;
        int anno=0;
        int i=1;

        if(c[i]!='/') {
            giorno = giorno * 10 + (c[i] - C);
            i++;
        }

        i++;
        mese=c[i]-C;
        i++;

        if(c[i]!='/'){
            mese = mese * 10 + (c[i] - C);
            i++;
        }
        i++;

        for(int j=3;j>=0;j--){
            anno+=(c[i]-C)*Math.pow(10,j);
            i++;
        }


        Integer[] res = new Integer[3];
        res[0]=giorno;
        res[1]=mese;
        res[2]=anno;

        return res;
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString("citta",cittaCheck);
    }
}