package sms1920.buybuycoupon;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import sms1920.buybuycoupon.model.Negozio;


/**
 * Adapter con i dati relativi ai negozi
 */
public class NegoziAdapter extends RecyclerView.Adapter<NegoziAdapter.ViewHolder> implements Filterable {

    private Context context;
    private ArrayList<Negozio> negozi;
    private ArrayList<Negozio> negoziFull;
    private FirebaseStorage storage = FirebaseStorage.getInstance();

    NegoziAdapter(ArrayList<Negozio> n){
        this.negozi = n;
        negoziFull = new ArrayList<>(negozi);
        //array paralleli per la gestione della ricerca in tempo reale
    }

    @NonNull
    @Override
    public NegoziAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_negozio,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        context = parent.getContext();
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final NegoziAdapter.ViewHolder holder, final int position) {

        if(negozi.size()==0)
            //gestione del caso in cui non ci siano negozi da mostrare
            holder.subtitleCardTextView.setText(context.getResources().getString(R.string.no_shops));
        else {
            //recupero un negozio per volta
            Negozio n = negozi.get(position);

            // Create a storage reference from our app
            StorageReference fileRef = storage.getReferenceFromUrl("gs://bbcoupon-e99f6.appspot.com").child(n.getImmagine());
            //imposta il percorso dell'immagine del negozio per recuperarla dallo storage remoto


            try {
                final File localFile = File.createTempFile("images", "jpg");
                //crea un file locale temporaneo per salvare l'immagine del negozio

                fileRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                        Glide.with(context).load(bitmap).centerCrop().into(holder.imageView);
                        //imposta l'immagine nella card
                    }
                });

            } catch (IOException e) {
                e.printStackTrace();
            }

            //mostro i dati del negozio
            holder.titleCardTextView.setText(n.getNomeNegozio());
            holder.subtitleCardTextView.setText(n.getVia() + ", " + n.getCivico() + " | " + (n.getCitta()));
            holder.textCardTextView.setText(context.getResources().getString(R.string.field) + ": " + n.getSettore());
        }
    }

    @Override
    //numero degli elementi della recyclerview
    public int getItemCount() {
        if(negozi.size()==0)
            //nel caso in cui non ci siano negozi, mostro comunque una card
            return 1;
        return negozi.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            //filtro i dati sulla rierca in tempo reale
            ArrayList<Negozio> listaFiltrata = new ArrayList<>();

            if (charSequence == null || charSequence.length() == 0)
                listaFiltrata.addAll(negoziFull);
            else {
                String filterPattern = charSequence.toString().toLowerCase().trim();

                for (Negozio n : negoziFull)
                    //i dati vengono filtrati sulla base del nome del negozio come idea iniziale
                    //poi sono stati aggiunti anche il settore e la descrizione, per permettere
                    //una ricerca meno restrittiva
                    if(n.getNomeNegozio().toLowerCase().contains(filterPattern)||
                            n.getSettore().toLowerCase().contains(filterPattern)||
                            n.getDescrizione().toLowerCase().contains(filterPattern))
                        listaFiltrata.add(n);
            }

            FilterResults risultati = new FilterResults();
            risultati.values = listaFiltrata;

            return risultati;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            negozi.clear();
            negozi.addAll((ArrayList<Negozio>) filterResults.values);
            //mostro i risultati della ricerca nella recyclerView
            notifyDataSetChanged();
        }
    };


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleCardTextView;
        TextView subtitleCardTextView;
        TextView textCardTextView;
        ImageView imageView;
        CardView cardNegozio;

        ViewHolder(View itemView)
        {
            super(itemView);

            //inizializzazione elementi della card del negozio
            cardNegozio = itemView.findViewById(R.id.negozioCard);
            imageView = itemView.findViewById(R.id.fotoNegozio);
            titleCardTextView = itemView.findViewById(R.id.titleCardTextView);
            subtitleCardTextView = itemView.findViewById(R.id.subtitleCardTextView);
            textCardTextView = itemView.findViewById(R.id.textCardTextView);


            if(negozi.size()==0) {
                //nel caso in cui non ci siano negozi nascondo l'imageView...
                imageView.setVisibility(View.GONE);
                //...e la card non è cliccabile
                itemView.setClickable(false);
            }
            else {

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //gestione del tocco della card
                        Intent intent = new Intent(context, SplashActivity.class);

                        // passaggio dei negozio e dell'id del negozio scelto all'activity di dettaglio del negozio
                        intent.putExtra("negozio", negozi.get(getAdapterPosition()));
                        intent.putExtra("destinazione", "coupon");
                        Log.e("negoziAdapter",HomeActivity.getCitta());
                        intent.putExtra("citta",HomeActivity.getCitta());
                        ActivityCompat.startActivity(context, intent, null);
                        try {
                            NegoziAdapter.this.finalize();
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    }
                });
            }
        }
    }
}
