package sms1920.buybuycoupon;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import sms1920.buybuycoupon.model.Utente;

/**
 * Activity per la registrazione base degli utenti
 */
public class SignUpActivity extends AppCompatActivity {

    private FirebaseFirestore fStore;
    private final int[] data = new int[]{0, 0, 0};
    private static final int PICK_IMAGE = 1;
    private ImageView image;
    private Bitmap bitmap;
    private Utente utente = new Utente();
    private boolean immagineNonCaricata = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        final TextView dataNascita = findViewById(R.id.DataNascitaUtente);
        final Calendar calendario = Calendar.getInstance();

        Spinner spinner = findViewById(R.id.spinnerContinuaCome);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                //aggiorna il campo in base alla selezione dello spinner
                TextView txt= arg1.findViewById(R.id.rowtext);
                updateRuolo(txt.getText().toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
                //di default si pensa che il ruolo scelto sia di cliente
                updateRuolo(getResources().getString(R.string.customer));
            }
        });

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,R.layout.row,
                new String[]{getResources().getString(R.string.customer),getResources().getString(R.string.dealer)});
        spinner.setAdapter(arrayAdapter);
        //adapter al volo per lo spinner, formato unicamente dai due ruoli

        final Spinner sesso = findViewById(R.id.spinnerSessoUtente);
        final ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<>(this,R.layout.row,
                new String[]{getResources().getString(R.string.m),getResources().getString(R.string.f),getResources().getString(R.string.other)});
        sesso.setAdapter(stringArrayAdapter);
        //adapter al volo per lo spinner, formato unicamente da M,F o altro.

        sesso.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                TextView txt= arg1.findViewById(R.id.rowtext);
                updateSex(txt.getText().toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
                updateSex(getResources().getString(R.string.m));
            }
        });

        //datapicker
        dataNascita.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int giorno = calendario.get(Calendar.DAY_OF_MONTH);
                int mese = calendario.get(Calendar.MONTH);
                int anno = calendario.get(Calendar.YEAR);

                final DatePickerDialog datePickerDialog;

                datePickerDialog = new DatePickerDialog(SignUpActivity.this, 0, new DatePickerDialog.OnDateSetListener()
                {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        dataNascita.setText(dayOfMonth + " / " + (month + 1) + " / " + year);
                        data[0]=dayOfMonth;
                        data[1]=month+1;
                        data[2]=year;
                        utente.setDataNascita(""+data[0]+"/"+data[1]+"/"+data[2]);
                    }
                }, anno, mese, giorno);

                datePickerDialog.show();
            }
        });

        Button avanti = findViewById(R.id.BT_avanti);
        avanti.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        FirebaseAuth fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();

        utente.setEmail(fAuth.getCurrentUser().getEmail());
        utente.setIdUtente(fAuth.getCurrentUser().getUid());

        image = findViewById(R.id.proPic);
        Button upload = findViewById(R.id.uploadProPic);
        upload.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //verifica del permesso di accesso allo storage
                if(ContextCompat.checkSelfPermission(SignUpActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                    ActivityCompat.requestPermissions(SignUpActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},PICK_IMAGE);
                else {
                    //se si ha il permesso di accedere allo storage,
                    //allora al tocco del pulsante apre l'internt per
                    //scegliere l'immagine da caricare.
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_IMAGE);
                }
            }
        });

        avanti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;

                //controlli sui dati inseriti
                if(data[0]==0){
                    Toast.makeText(SignUpActivity.this,getResources().getString(R.string.dataNull),Toast.LENGTH_SHORT).show();
                    return;
                }

                if(checkData()){
                    Toast.makeText(SignUpActivity.this,getResources().getString(R.string.dataWrong),Toast.LENGTH_LONG).show();
                    return;
                }

                if(immagineNonCaricata){
                    utente.setImmagine("nothing");
                }

                if(utente.getTipo().equals("Customer")||utente.getTipo().equals("Cliente")) {
                    //se il ruolo scelto è "cliente" allora registra i dati nel database
                    intent = new Intent(SignUpActivity.this, MainActivity.class);
                    caricaDati();
                }
                else intent = new Intent(SignUpActivity.this, SignUpActivityShop.class);
                utente.setTipo("Dealer");
                intent.putExtra("utente",utente);
                //passaggio dei dati all'activity per la registrazione del negozio
                startActivity(intent);
                finish();
            }
        });
    }

    private void caricaDati() {
        fStore = FirebaseFirestore.getInstance();

        // Crea un mappa con i dati dell'utente
        Map<String, Object> user = new HashMap<>();
        user.put("email", utente.getEmail());
        user.put("sesso", utente.getSesso());
        user.put("data", utente.getDataNascita() );
        user.put("immagine",utente.getImmagine());
        user.put("tipo",utente.getTipo());

        // Add a new document with the current user ID
        fStore.collection("users").document(utente.getIdUtente())
                .set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("", "DocumentSnapshot added with ID: " );
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("", "Error adding document", e);
                    }
                });
    }

    private void updateSex(String toString) {
        utente.setSesso(toString);
    }
    private void updateRuolo(String toString) {
        utente.setTipo(toString);
    }

    /**
     * Metodo che gestisce la risposta dell'utente alla richiesta del permesso di accesso all'archivio
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //se abbiamo il permesso di accedere all'archivio, mostriamo l'intent implicito
            //per la scelta dell'immagine
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, PICK_IMAGE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && null != data) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();

                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                decodeFile(picturePath);

                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRef = storage.getReference();
                //salvo il percorso dell'immagine del profilo caricata
                utente.setImmagine("images/"+ selectedImage.getLastPathSegment());
                StorageReference riversRef = storageRef.child(utente.getImmagine());
                immagineNonCaricata = false;

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                UploadTask uploadTask = riversRef.putFile(selectedImage);

                // Register observers to listen for when the download is done or if it fails
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.e("upload:","failed");
                        // Handle unsuccessful uploads
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Log.e("upload:","success");
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                        // ...
                    }
                });
            }
        }
    }

    public void decodeFile(String filePath) {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        bitmap = BitmapFactory.decodeFile(filePath, o2);

        image.setImageBitmap(bitmap);
    }

    private boolean checkData(){
        //controllo sulla data di nascita immessa
        boolean res = false;
        Date date = new Date();
        int g=date.getDate();
        int m=date.getMonth()+1;
        int a=date.getYear()+1900;
        int anno = data[2];
        int mese = data[1];
        int giorno = data[0];

        if(anno>a-14||anno<a-100)
            res=true;
        else if(anno==a){
            if(mese>m)
                res=true;
            else
            if(mese==m&&giorno>g)
                res=true;


        }
        return res;
    }

    @Override
    public void onBackPressed() {
        //non si può tornare indietro
        //possibile soluzione: logout
    }
}