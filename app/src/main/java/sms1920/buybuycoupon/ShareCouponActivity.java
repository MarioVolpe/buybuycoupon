package sms1920.buybuycoupon;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import sms1920.buybuycoupon.model.Coupon;

public class ShareCouponActivity extends AppCompatActivity {

    private TextView status;
    private ImageButton btnConnect;
    private Dialog dialog;
    private BluetoothAdapter bluetoothAdapter;
    private String chiamante;
    private String codiceCoupon ="";
    private Coupon coupon;
    private String idUtente="";
    private TextView idCliente;
    private FirebaseAuth fAuth;
    private Button compareButton;

    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_OBJECT = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final String DEVICE_OBJECT = "device_name";


    private static final int REQUEST_ENABLE_BLUETOOTH = 1;
    private static final int REQUEST_ID_FINE_LOCATION = 9;
    private BTController btController;
    private BluetoothDevice connectingDevice;
    private ArrayAdapter<String> discoveredDevicesAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //inizializzazione views
        setContentView(R.layout.activity_share_coupon);
        status = findViewById(R.id.statusTextInputEditText);
        btnConnect = findViewById(R.id.connectButton);
        TextView couponRichiesto = findViewById(R.id.couponRichiesto);
        idCliente = findViewById(R.id.idCliente);
        fAuth = FirebaseAuth.getInstance();
        compareButton = findViewById(R.id.compareButton);
        compareButton.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        //prende i dettagli del coupon dal chiamante e il nome del chiamante
        //quest'ultimo serve per personalizzare la vista (cliente o negoziante)
        coupon = (Coupon) getIntent().getExtras().get("coupon");
        chiamante = getIntent().getExtras().getString("chiamante");

        if(chiamante.equals("cliente")){
            //vista cliente
            compareButton.setText(getString(R.string.reqCouponMsg));
            //l'id del cliente viene mostrato solo al negoziante
            idCliente.setVisibility(View.INVISIBLE);
            //recupera dall'activity chiamante l'id dell'utente che viene passato tramite bluetooth
            idUtente = getIntent().getStringExtra("idUtente");
            couponRichiesto.setText(getString(R.string.btReqMsg)+"\n"+coupon.getNome()+"\n"
                    +coupon.getDescrizione()+"\n"+getString(R.string.actBT));
        } else {
            //vista negoziante
            compareButton.setText(getString(R.string.genCouponMsg));
            //il bottone è disabilitato, si attiva dopo la richiesta
            compareButton.setEnabled(false);
            //generazione dell'id del coupon
            codiceCoupon=String.valueOf(UUID.randomUUID());
            couponRichiesto.setText(getString(R.string.btGenMsg)+"\n"+coupon.getNome()+"\n"
                    +coupon.getDescrizione()+"\n"+getString(R.string.actBT));
        }


        compareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //tasto per la richiesta/generazione
                if(chiamante.equals("cliente")) {
                    //vista cliente
                    sendMessage(fAuth.getCurrentUser().getUid());
                    //manda l'id come messaggio e disabilita il bottone
                    compareButton.setEnabled(false);
                }
                else {
                    //dopo aver ricevuto l'id, il bottone è attivo
                    //manda al cliente il codice del coupon generato
                    sendMessage(codiceCoupon + "");
                    //salvataggio del coupon generato nel db
                    salvaCoupon();

                    //mostra il messaggio del coupon generato
                    AlertDialog.Builder alert = new AlertDialog.Builder(ShareCouponActivity.this);

                    alert.setTitle(getString(R.string.genCoupon));

                    alert.setMessage(getString(R.string.genCouponCode) + "\n" + codiceCoupon);
                    alert.setPositiveButton(getString(R.string.ok), null);
                    alert.show();

                }
            }
        });

        askPermissionLocation();

        //check device support bluetooth or not
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Toast.makeText(this, "BT not available", Toast.LENGTH_SHORT).show();
            finish();
        }

        //show bluetooth devices dialog when click connect button
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPrinterPickDialog();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Note: If request is cancelled, the result arrays are empty.
        if (grantResults.length <= 0) {
            Toast.makeText(getApplicationContext(),getString(R.string.permCanc), Toast.LENGTH_SHORT).show();
        }
    }

    private void askPermissionLocation() {
        boolean canLocation = this.askPermission(REQUEST_ID_FINE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION);

        if (!canLocation) {
            Toast.makeText(getApplicationContext(),getString(R.string.sharedCouponBTNeeded), Toast.LENGTH_LONG).show();
        }
    }

    private boolean askPermission(int requestId, String permissionName) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {

            // Check if we have permission
            int permission = ActivityCompat.checkSelfPermission(this, permissionName);


            if (permission != PackageManager.PERMISSION_GRANTED) {
                // If don't have permission so prompt the user.
                this.requestPermissions(
                        new String[]{permissionName},
                        requestId
                );
                return false;
            }
        }
        return true;
    }

    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    //gestione della connessione tra dispositivi
                    switch (msg.arg1) {
                        case BTController.STATE_CONNECTED:
                            setStatus(getString(R.string.connected_to) + " "+connectingDevice.getName());
                            btnConnect.setEnabled(false);
                            if(chiamante.equals("cliente"))
                                compareButton.setEnabled(true);
                            break;
                        case BTController.STATE_CONNECTING:
                            setStatus(getString(R.string.connecting));
                            btnConnect.setEnabled(false);
                            break;
                        case BTController.STATE_LISTEN:
                        case BTController.STATE_NONE:
                            setStatus(getString(R.string.not_connected));
                            btnConnect.setEnabled(true);
                            if(chiamante.equals("cliente"))
                                compareButton.setEnabled(false);
                            break;
                    }
                    break;

                case MESSAGE_WRITE:
                    //dopo l'invio del messaggio
                    byte[] writeBuf = (byte[]) msg.obj;

                    String writeMessage = new String(writeBuf);

                    break;

                case MESSAGE_READ:
                    //gestione del messaggio ricevuto
                    byte[] readBuf = (byte[]) msg.obj;

                    String readMessage = new String(readBuf, 0, msg.arg1);

                    if(!chiamante.equals("cliente")) {
                        //abilita il pulsante per la generazione del coupon
                        compareButton.setEnabled(true);
                        idUtente = readMessage;
                        //mostra l'id dell'utente che richiede il coupon
                        idCliente.setText("ID Cliente: \n" + readMessage);

                    } else {

                        AlertDialog.Builder alert = new AlertDialog.Builder(ShareCouponActivity.this);
                        //mostra al cliente l'id del coupon ricevuto
                        alert.setTitle(getString(R.string.genCoupon));
                        alert.setMessage(getString(R.string.your_coupon_code_is) + "\n" + readMessage + "\n" + getString(R.string.retireCoupon));
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }

                    break;
                case MESSAGE_DEVICE_OBJECT:
                    connectingDevice = msg.getData().getParcelable(DEVICE_OBJECT);
                    Toast.makeText(getApplicationContext(), getString(R.string.connected_to) + " " + connectingDevice.getName(),
                            Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(getApplicationContext(), msg.getData().getString("toast"),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
            return false;
        }
    });

    private void showPrinterPickDialog() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_bluetooth);
        dialog.setTitle(getString(R.string.bluetooth_devices));

        if (bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }
        bluetoothAdapter.startDiscovery();

        //Initializing bluetooth adapters
        ArrayAdapter<String> pairedDevicesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        discoveredDevicesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);

        //locate listviews and attatch the adapters
        ListView listView = (ListView) dialog.findViewById(R.id.pairedDeviceList);
        ListView listView2 = (ListView) dialog.findViewById(R.id.discoveredDeviceList);
        listView.setAdapter(pairedDevicesAdapter);
        listView2.setAdapter(discoveredDevicesAdapter);

        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(discoveryFinishReceiver, filter);

        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(discoveryFinishReceiver, filter);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        // If there are paired devices, add each one to the ArrayAdapter
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                pairedDevicesAdapter.add(device.getName() + "\n" + device.getAddress());
            }
        } else {
            pairedDevicesAdapter.add(getString(R.string.none_paired));
        }

        //Handling listview item click event
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                bluetoothAdapter.cancelDiscovery();
                String info = ((TextView) view).getText().toString();
                String address = info.substring(info.length() - 17);

                connectToDevice(address);
                dialog.dismiss();
            }

        });

        listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                bluetoothAdapter.cancelDiscovery();
                String info = ((TextView) view).getText().toString();
                String address = info.substring(info.length() - 17);

                connectToDevice(address);
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    private void setStatus(String s) {
        status.setText(s);
    }

    private void connectToDevice(String deviceAddress) {
        bluetoothAdapter.cancelDiscovery();
        BluetoothDevice device = bluetoothAdapter.getRemoteDevice(deviceAddress);
        btController.connect(device);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
            if (resultCode == Activity.RESULT_OK) {
                //inizializzazione controller
                btController = new BTController(this, handler);
            } else {
                Toast.makeText(this, getString(R.string.bluetooth_disabled), Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private void sendMessage(String message) {
        if (btController.getState() != BTController.STATE_CONNECTED) {
            Toast.makeText(this, getString(R.string.connection_lost), Toast.LENGTH_SHORT).show();
            return;
        }

        if (message.length() > 0) {
            //invio del messaggio
            byte[] send = message.getBytes();
            btController.write(send);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!bluetoothAdapter.isEnabled()) {
            //chiede di abilitare il bluetooth se non attivo
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BLUETOOTH);
        } else {
            btController = new BTController(this, handler);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (btController != null) {
            if (btController.getState() == BTController.STATE_NONE) {
                //lancia il controller
                btController.start();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (btController != null)
            //ferma il controller
            btController.stop();
    }

    private final BroadcastReceiver discoveryFinishReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            //gestione dell'accoppiamento

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    discoveredDevicesAdapter.add(device.getName() + "\n" + device.getAddress());
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                if (discoveredDevicesAdapter.getCount() == 0) {
                    discoveredDevicesAdapter.add(getString(R.string.none_found));
                }
            }
        }
    };

    private void salvaCoupon() {
        //inserisce nel db il coupon effettivo

        FirebaseFirestore fStore = FirebaseFirestore.getInstance();

        Date date = new Date();
        int g=date.getDate();
        int m=date.getMonth()+1;
        int a=date.getYear()+1900;
        String data = ""+g+"/"+m+"/"+a;
        //recupero la data di oggi dal sistema

        // Crea un mappa con i dati del coupon generato
        Map<String, Object> couponEffettivo = new HashMap<>();
        couponEffettivo.put("cliente",idUtente);
        couponEffettivo.put("negozio", coupon.getNegozio());
        couponEffettivo.put("coupon", coupon.getIdCoupon());
        couponEffettivo.put("data",data);

        // Add a new document with with the current user ID
        fStore.collection("activeCoupons").document(codiceCoupon)
                .set(couponEffettivo)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("", "DocumentSnapshot added with ID: "+codiceCoupon);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("", "Error adding document", e);
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        super.finish();
        Intent intent = new Intent(ShareCouponActivity.this,SplashActivity.class);
        intent.putExtra("destinazione","home");
        intent.putExtra("citta",getIntent().getExtras().getString("citta"));
        startActivity(intent);
        finish();
    }
}